/**
 * functional interface
 */
public interface Printable {
    void print();
}
