import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * class prints out and deals with main menu
 */
public class ConsoleMenu {
    private Map<Integer, String> menu = new LinkedHashMap<>();
    private Map<Integer, Printable> methods = new LinkedHashMap<>();

    /**
     * entry point
     *
     * @param args main arguments
     */
    public static void main(String[] args) {
        ConsoleMenu consoleMenu = new ConsoleMenu();
        consoleMenu.initMaps();
        while (true) {
            System.out.println("Please, make your choice: ");
            consoleMenu.printMenu();
            consoleMenu.readInput();
        }
    }

    /**
     * method initializes "menu" and "methods"
     */
    private void initMaps() {
        menu.put(1, "Option #1");
        menu.put(2, "Option #2");
        menu.put(3, "Option #3");
        menu.put(4, "Option #4");
        menu.put(0, "Exit");

        methods.put(1, this::startOption1);
        methods.put(2, this::startOption2);
        methods.put(3, this::startOption3);
        methods.put(4, this::startOption4);
        methods.put(0, this::startOption0);
    }

    /**
     * method prints out the main menu
     */
    private void printMenu() {
        for (Map.Entry<Integer, String> map : menu.entrySet()) {
            System.out.println(map.getKey() + " " + map.getValue());
        }
    }

    /**
     * method deals with user's input
     */
    private void readInput() {
        Scanner scanner = new Scanner(System.in);
        int res = scanner.nextInt();
        while (res < 0 || res > menu.size() - 1) {
            System.out.println("You entered the wrong number. Please, try again...");
            res = scanner.nextInt();
        }
        methods.get(res).print();
    }

    /**
     * method performs some action
     */
    private void startOption1() {
        System.out.println("Option1 started");
    }

    /**
     * method performs some action
     */
    private void startOption2() {
        System.out.println("Option2 started");
    }

    /**
     * method performs some action
     */
    private void startOption3() {
        System.out.println("Option3 started");
    }

    /**
     * method performs some action
     */
    private void startOption4() {
        System.out.println("Option4 started");
    }

    /**
     * method ends the program
     */
    private void startOption0() {
        System.exit(0);
    }
}
